//
//  VTNodeDevice+Verigard.h
//  Node_iOS
//
//  Created by Andrew T on 5/4/17.
//  Copyright © 2017 Variable, Inc. All rights reserved.
//

#import "VTNodeDevice.h"

@interface VTNodeDevice (Verigard)

/** Request VG setting (int/gain) - (response via nodeDeviceDidTransmitVGSettings:)
 */
-(void)vgRequestIntegrationGainSettings;


/** Set VG setting (int/gain) to be used all scans in button press/BLE requests
 @param scale - Celcius / Fahrenheit
 */
-(void)vgSetIntegrationTime:(uint8_t)integrationTime gain:(VTNodeVgGain)gain;


@end
