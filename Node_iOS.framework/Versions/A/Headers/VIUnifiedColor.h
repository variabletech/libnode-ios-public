//
//  VIUnifiedColor.h
//  Node_iOS
//
//  Created by Andrew T on 6/2/17.
//  Copyright © 2017 Variable, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VTColorUtils.h"
#import "VTLabColor.h"
#import "VTXYZColor.h"
#import "VTRGBColor.h"

@interface VIUnifiedColor : NSObject

/** Init VIUnifiedColor class method
 @param unadjLab unadjusted unified lab.
 @param batchNum - target batch number (NSNumber: intValue).
 @return A VIUnifiedColor object
 */
+(VIUnifiedColor*)colorWithLab:(VTLabColor*)unadjLab batchNum:(NSNumber*)batchNum;


/** Get a CIE Lab Color Value for object's target batch
 @param illuminant D50 or D65 reference illuminant (D50 is default for Color Match)
 @return A VTLabColor CIE Lab Color object
 */
-(VTLabColor*)getUnifiedLab:(VTColorUtilsIlluminant)illuminant;


/** Get the ColorReading's Target Batch #
 @return NSNumber (intValue) of the batch #. If nil, then no shift performed.
 */
-(NSNumber*)getTargetBatchNumber;


@end

