//
//  VTNodeDevice+Photon.h
//  Node_iOS
//
//  Created by Corey Mann on 1/15/16.
//  Copyright © 2016 Variable, Inc. All rights reserved.
//

#ifndef VTNodeDevice_Photon_h
#define VTNodeDevice_Photon_h

@interface VTNodeDevice(Photon)

-(void) requestPhotonReadingWithIntegrationTime: (unsigned int) integrationTime WithGain: (unsigned int) gain withLED: (unsigned int) led;

/**
 * Request the color spectrum.
 *
 */
-(void) requestSpectrum;

/**
 * Request a photon white point calibration. This MUST be called before any photon can be scanned.
 */
-(void) requestPhotonWhitepointCal;


/** Initialize a Photon device via serial (Photon Device doesn't necessarily have to be attached)
 @param serial Serial# of the Photon Module to register.
 @param success Callback block that will reflect Chroma ready
 @param fail Callback block that indicates something went wrong
 */
+(void)photonInitWithSerial:(NSString*)serial success:(void(^)(BOOL chromaReady))success fail:(void(^)(NSError *error))fail;

@end
#endif /* VTNodeDevice_Photon_h */
