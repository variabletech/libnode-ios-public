//
//  VTIOModuleViewController.m
//  api-demo-io_module
//
//  Created by Andrew T on 9/25/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//


/******************************************************************************************************************
 IO Module Details:
 WARNING: NODE1 only supports connection to a single IO Module!!!
 
 The IO Module contains: 
 -8 GPIO pins
 -2 A2D inputs
 -4 pins which currently are set up to be used for UART
 
 Functionality:
 8 GPIO pins can be configured as Input, Output, or IRQ
    -Output pins are set using the same method that polls Input pins: VTNodeDevice's: setIOModuleIOPins, the corresponding delegate is: didTransmitIOModulePins
    -IRQ pin going high will automatically send the interrupt source pin to VTNodeDevice's delegate: didTransmitIOModuleIrq
    -IRQ is cleared only upon a read of the GPIO pins.
 A2D values are read using requestIOModuleA0 or requestIOModuleA1, with the response coming back to the delegate method: didTransmitIOModuleA2D
 UART is covered in the IOUARTViewController class
 
 ******************************************************************************************************************/


#import "VTIOModuleViewController.h"
#import "VTAppDelegate.h"

@interface VTIOModuleViewController()
{
    VTNodeDevice *connectedNode;
    NSArray *ioPinSettingButtons;   //Array of pin settings (IRQ, Input, or Output)
    NSArray *ioPinSwitchArray;      //Array to keep track of pinstates
    
    NSTimer *myTimer;
}

@end

@implementation VTIOModuleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    ioPinSettingButtons = [[NSArray alloc] initWithObjects:self.io7SettingsButton,self.io6Button,self.io5Setting,self.io4Setting,self.io3Setting,self.io2Setting,self.io1Setting,self.io0Setting, nil];
    ioPinSwitchArray= [[NSArray alloc] initWithObjects:self.io7Switch,self.io6Switch,self.io5Switch,self.io4Switch,self.io3Switch,self.io2Switch,self.io1Switch,self.io0Switch,nil];
}


- (void)viewDidAppear:(BOOL)animated
{
    connectedNode =[VTNodeManager getInstance].selectedNodeDevice;
    connectedNode.delegate = self;
    if ((connectedNode.module_a_type != VTNodeModuleTypeIOModule) && (connectedNode.module_b_type != VTNodeModuleTypeIOModule)) return;
    NSMutableArray *arrayOfPinTypes = [[NSMutableArray alloc] init];
    for (UIButton *button in ioPinSettingButtons)
    {
        //@page load, set all pins will as inputs.
        [button setTitle:@"Input" forState:UIControlStateNormal];
        [button setTitle:@"Input" forState:UIControlStateSelected];
        [arrayOfPinTypes addObject:[NSNumber numberWithInt:VTNodeGPI]];
        ((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]).enabled = NO;
        ((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]).on = NO;
    }
    [connectedNode setIOModuleIOPinConfig:arrayOfPinTypes];
    [self gpioSwitchesUpdated:nil];
   // [connectedNode requestIOModuleStreaming:YES withA0:YES withA1:YES];
    [self startTimer];
}

- (void)startTimer {
    //ensure no extraneous timers:
    [self stopTimer];
    // create timer on run loop
    myTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timerTicked:) userInfo:nil repeats:YES];
}
-(void)timerTicked:(NSTimer*)timer {  [self requestA2DData]; }
-(void)stopTimer
{
    [myTimer invalidate];
    myTimer = nil;
}
-(void)requestA2DData
{
    [connectedNode requestIOModuleA0];
    [connectedNode requestIOModuleA1];
}




-(void) viewWillDisappear:(BOOL)animated {
    [connectedNode requestIOModulePowerDown];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updatePinSettingFor:(uint8_t)pinNum
{
    NSString *title = [NSString stringWithFormat:@"Select Setting for Pin %d",pinNum];
    NSInteger tag = (NSInteger)7-pinNum;
    
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:title message:@"I/O or IRQ" preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"Input" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self updateBtns:tag title:@"Input"];
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:@"Output" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self updateBtns:tag title:@"Output"];
    }]];
    [ac addAction:[UIAlertAction actionWithTitle:@"IRQ" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self updateBtns:tag title:@"IRQ"];
    }]];
    
    [ac addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:ac animated:true completion:nil];
}

-(void)updateBtns:(NSInteger)tag title:(NSString*)buttonTitle
{
    [[ioPinSettingButtons objectAtIndex:tag] setTitle:buttonTitle forState:UIControlStateNormal];
    [[ioPinSettingButtons objectAtIndex:tag] setTitle:buttonTitle forState:UIControlStateSelected];
    NSMutableArray *arrayOfPinTypes = [[NSMutableArray alloc] init];
    for (UIButton *button in ioPinSettingButtons)
    {
        if ([button.currentTitle isEqualToString:@"IRQ"])
        {
            ((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]).enabled = NO;
            [((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]) setOnTintColor:[UIColor redColor]];
            [arrayOfPinTypes addObject:[NSNumber numberWithInt:VTNodeIRQ]];
        }
        else if ([button.currentTitle isEqualToString:@"Input"])
        {
            ((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]).enabled = NO;
            [((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]) setOnTintColor:[UIColor blueColor]];
            [arrayOfPinTypes addObject:[NSNumber numberWithInt:VTNodeGPI]];
        }
        else //output
        {
            ((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]).enabled = YES;
            [((UISwitch*)[ioPinSwitchArray objectAtIndex:[ioPinSettingButtons indexOfObject:button]]) setOnTintColor:[UIColor blueColor]];
            [arrayOfPinTypes addObject:[NSNumber numberWithInt:VTNodeGPO]];
        }
    }
    [connectedNode setIOModuleIOPinConfig:arrayOfPinTypes];
}


- (IBAction)io7SettingPressed:(id)sender {
    [self updatePinSettingFor:7];
}
- (IBAction)io6SettingPressed:(id)sender {
    [self updatePinSettingFor:6];
}
- (IBAction)io5SettingPressed:(id)sender {
    [self updatePinSettingFor:5];
}
- (IBAction)io4SettingPressed:(id)sender {
    [self updatePinSettingFor:4];
}
- (IBAction)io3SettingPressed:(id)sender {
    [self updatePinSettingFor:3];
}
- (IBAction)io2SettingPressed:(id)sender {
    [self updatePinSettingFor:2];
}
- (IBAction)io1SettingPressed:(id)sender {
    [self updatePinSettingFor:1];
}
- (IBAction)io0SettingPressed:(id)sender {
    [self updatePinSettingFor:0];
}
- (IBAction)gpioSwitchesUpdated:(id)sender {
    
    uint8_t d7_to_d0 = 0x00;
    uint8_t writeMask = 0x00;
    
    for (int i=0;i<8;i++)
    {
        if ([((UISwitch*)[ioPinSwitchArray objectAtIndex:i]) isOn])
            d7_to_d0 |= (1<<(7-i));
    }
    for (int i=0;i<8;i++)
    {
        if ( [[[ioPinSettingButtons objectAtIndex:i] currentTitle]  isEqualToString:@"Output"])
            writeMask |= (1<<(7-i));
    }
    
    [connectedNode setIOModuleIOPins:writeMask byteToWrite:d7_to_d0];
    //Whenever setting/requesting GPIO settings, go ahead and request A2D Values
    [connectedNode requestIOModuleA0];
    [connectedNode requestIOModuleA1];
}



#pragma mark ---- NODE Delegate Methods ----

-(void)nodeDevice:(VTNodeDevice *)device didTransmitIOModulePins:(uint8_t)d7_to_d0
{
    for (int i=0;i<8;i++)
    {
        if ( ((1<<(7-i)) & d7_to_d0) == (1<<(7-i)) )
            [((UISwitch*)[ioPinSwitchArray objectAtIndex:i]) setOn:YES animated:YES];
        else
            [((UISwitch*)[ioPinSwitchArray objectAtIndex:i]) setOn:NO animated:YES];
    }
}

-(void)nodeDevice:(VTNodeDevice *)device didTransmitIOModuleIrq:(uint8_t)trigger_pins
{
    for (int i=0;i<8;i++)
    {
        if ( ((1<<(7-i)) & trigger_pins) == (1<<(7-i)) )
        {
            [((UISwitch*)[ioPinSwitchArray objectAtIndex:i]) setOn:YES animated:YES];
        }
    }
}

-(void)nodeDevice:(VTNodeDevice *)device didTransmitIOModuleA2D:(uint8_t)a0_or_a1 value:(float)value
{
    if (!a0_or_a1)
        [self.a0Slider setValue:value];
    else
        [self.a1Slider setValue:value];
}



@end
