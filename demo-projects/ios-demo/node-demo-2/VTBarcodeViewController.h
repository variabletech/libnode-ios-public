//
//  VTBarcodeViewController.h
//  node-api-demo-public
//
//  Created by Andrew T on 6/23/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTBarcodeViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UISwitch *switchEnableNodeBtnBarcode;

@property (strong, nonatomic) IBOutlet UITextField *txtScan;
- (IBAction)btnScanPressed:(id)sender;
- (IBAction)switchEnableNodeBtnChanged:(id)sender;



@end
