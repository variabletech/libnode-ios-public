//
//  VTClimaViewController.m
//  node-demo-2
//
//  Created by Wade Gasior on 10/24/12.
//  Copyright (c) 2012 Variable Technologies. All rights reserved.
//

#import "VTClimaViewController.h"

@interface VTClimaViewController () <NodeDeviceDelegate>

@end

@implementation VTClimaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    //Grab the Node delegate
    [VTNodeManager getInstance].selectedNodeDevice.delegate = self;
    [[VTNodeManager getInstance].selectedNodeDevice setStreamModeClimaTP:YES Humidity:YES LightProximity:YES withTimestampingEnabled:NO];
}

-(void) viewWillDisappear:(BOOL)animated {
    //Check if the back button was pressed
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        //Stop streaming
        [[VTNodeManager getInstance].selectedNodeDevice setStreamModeClimaTP:NO Humidity:NO LightProximity:NO withTimestampingEnabled:NO];
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NodeDeviceDelegate
-(void)nodeDeviceDidUpdateClimaHumidityReading:(VTNodeDevice *)device withReading:(float)reading {
    self.humidityLabel.text = [NSString stringWithFormat:@"%.2f %%", reading];
}

-(void)nodeDeviceDidUpdateClimaTempReading:(VTNodeDevice *)device withReading:(float)reading {
    self.temperatureLabel.text = [NSString stringWithFormat:@"%.2f C", reading];
}

-(void)nodeDeviceDidUpdateClimaPressureReading:(VTNodeDevice *)device withReading:(float)reading {
    self.pressureLabel.text = [NSString stringWithFormat:@"%.3f kPa", reading/1000.f];
}

-(void)nodeDeviceDidUpdateClimaLightReading:(VTNodeDevice *)device withReading:(float)reading {
    self.lightLevelLabel.text = [NSString stringWithFormat:@"%i lx", (int)reading];
}
@end
