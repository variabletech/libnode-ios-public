//
//  VTNodeConnectionManagerViewController.h
//  node-demo-2
//
//  Created by Wade Gasior on 10/22/12.
//  Copyright (c) 2012 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeManager.h"
#import "VTNodeDeviceCell.h"

@interface VTNodeConnectionManagerViewController : UITableViewController

@end
