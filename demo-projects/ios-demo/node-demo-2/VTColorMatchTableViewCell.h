//
//  VTColorMatchTableViewCell.h
//  node-api-demo-public
//
//  Created by Andrew T on 7/13/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTColorMatchTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *viewColor;
@property (strong, nonatomic) IBOutlet UILabel *lblLibrary;
@property (strong, nonatomic) IBOutlet UILabel *lblColorName;
@property (strong, nonatomic) IBOutlet UILabel *lblDeltaE;

@end
