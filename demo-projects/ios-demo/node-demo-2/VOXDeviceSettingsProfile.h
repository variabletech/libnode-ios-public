//
//  VOXDeviceSettingsProfile.h
//  NODE Oxa
//
//  Created by Tyler Brown on 7/23/13.
//  Copyright (c) 2013 Variable Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VTNodeManager.h"


@interface VOXDeviceSettingsProfile : NSObject

@property (nonatomic) VTNodeModuleSubType subModule;
@property (nonatomic, readonly) int   tiaGain;
@property (nonatomic, readonly) int   intZ;
@property (nonatomic, readonly) int   refSource;
@property (nonatomic, readonly) int   biasSign;
@property (nonatomic, readonly) int   biasValue;
@property (nonatomic, readonly) int   rLoad;
@property (nonatomic, readonly) int   opMode;
@property (nonatomic, readonly) int   shortingEFT;
@property (nonatomic, readonly) float responseRatio;

-(id) initWithSubModule:(VTNodeModuleSubType)subModule;

-(float)     tiaGainValueForSetting:(int)tiaSetting;
@end
