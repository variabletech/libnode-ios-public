//
//  IOUARTViewController.h
//  node-api-demo-public
//
//  Created by Andrew T on 10/1/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>

@interface IOUARTViewController : UIViewController <NodeDeviceDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *uartCommandField;

@property (strong, nonatomic) IBOutlet UITextView *uartIOTextField;

- (IBAction)sendCommand:(id)sender;


@end
