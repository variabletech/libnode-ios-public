//
//  VTThermaViewController.h
//  node-demo-2
//
//  Created by Wade Gasior on 10/24/12.
//  Copyright (c) 2012 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTThermaViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;

@end
