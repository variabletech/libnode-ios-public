//
//  VTOxaViewController.m
//  node-api-demo-public
//
//  Created by Andrew T on 10/3/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//

#import "VTOxaViewController.h"
#import "VOXDeviceSettingsProfile.h"
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>

@interface VTOxaViewController ()<NodeDeviceDelegate>
{
    VTNodeDevice *connectedNode;
    NSString *moduleALabelString;
    NSString *moduleBLabelString;
    NSNumber       *_currentABaseline;
    NSNumber       *_currentBBaseline;
    
}
@end

@implementation VTOxaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated {
    //Grab the Node delegate
    connectedNode =[VTNodeManager getInstance].selectedNodeDevice;
    connectedNode.delegate = self;
    
    //If we have 2 OXAs, display stream from both
    if (connectedNode.module_a_type == VTNodeModuleTypeOxa)
    {
        [connectedNode requestOxaBaselineForPort:VTModuleLocationA];
        switch (connectedNode.module_a_subtype) {
            case VTNodeModuleSubTypeCO:
                moduleALabelString = @"Port A CO:\n";
                break;
            case VTNodeModuleSubTypeCO2:
                moduleALabelString = @"Port A CO2:\n";
                break;
            case VTNodeModuleSubTypeNO:
                moduleALabelString = @"Port A NO:\n";
                break;
            case VTNodeModuleSubTypeCL2:
                moduleALabelString = @"Port A CL2:\n";
                break;
            case VTNodeModuleSubTypeNO2:
                moduleALabelString = @"Port A NO2:\n";
                break;
            case VTNodeModuleSubTypeSO2:
                moduleALabelString = @"Port A SO2:\n";
                break;
            case VTNodeModuleSubTypeH2S:
                moduleALabelString = @"Port A H2S:\n";
                break;
            default:
                moduleALabelString = @"Port A OXA:\n";
                break;
                
        }
        
    }
    else
    {
        moduleALabelString = @"Port A:\nNo OXA";
        self.oxaPortAReadingLabel.text = @"Port A:\nNo OXA";
        
    }
    
    if(connectedNode.module_b_type == VTNodeModuleTypeOxa)
    {
        [connectedNode requestOxaBaselineForPort:VTModuleLocationB];
        switch (connectedNode.module_b_subtype) {
            case VTNodeModuleSubTypeCO:
                moduleBLabelString = @"Port B CO:\n";
                break;
            case VTNodeModuleSubTypeCO2:
                moduleBLabelString = @"Port B CO2:\n";
                break;
            case VTNodeModuleSubTypeNO:
                moduleBLabelString = @"Port B NO:\n";
                break;
            case VTNodeModuleSubTypeCL2:
                moduleBLabelString = @"Port B CL2:\n";
                break;
            case VTNodeModuleSubTypeNO2:
                moduleBLabelString = @"Port B NO2:\n";
                break;
            case VTNodeModuleSubTypeSO2:
                moduleBLabelString = @"Port B SO2:\n";
                break;
            case VTNodeModuleSubTypeH2S:
                moduleBLabelString = @"Port B H2S:\n";
                break;
            default:
                moduleBLabelString = @"Port B OXA:\n";
                break;
        }
    }
    else
    {
        moduleBLabelString = @"Port B:\nNo OXA";
        self.oxaPortBReadingLabel.text = @"Port B:\nNo OXA";
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    if (connectedNode.module_a_type == VTNodeModuleTypeOxa)
        [connectedNode setStreamModeOxa:NO forModulePort:VTModuleLocationA withPeriod:25 withLifetime:0];
    if (connectedNode.module_b_type == VTNodeModuleTypeOxa)
        [connectedNode setStreamModeOxa:NO forModulePort:VTModuleLocationB withPeriod:25 withLifetime:0];
    [super viewWillDisappear:animated];
}


#pragma mark ----- NODE Delegate Methods ------
-(void)nodeDeviceDidUpdateOxaBaselineValue:(VTNodeDevice *)device withValue:(float)value forPort:(VTModuleLocation)port
{
    NSLog(@"OXA Baseline for port %d: %f",port,value);
    if (port == VTModuleLocationA){
        _currentABaseline = @(value);
        [connectedNode setStreamModeOxa:YES forModulePort:VTModuleLocationA withPeriod:25 withLifetime:0];
    }
    else if (port == VTModuleLocationB){
        _currentBBaseline = @(value);
        [connectedNode setStreamModeOxa:YES forModulePort:VTModuleLocationB withPeriod:25 withLifetime:0];
    }
    
}
-(void)nodeDeviceDidUpdateOxaReading:(VTNodeDevice *)device withReading:(float)reading forPort:(VTModuleLocation)port
{
    //reading is a raw sense value coming back from OXA, except for CO2, where the raw reading == PPM
    //Calculate PPM...
    if (port == VTModuleLocationA)
    {
        // get settings
        VOXDeviceSettingsProfile *settings = [[VOXDeviceSettingsProfile alloc] initWithSubModule:connectedNode.module_a_subtype];
        float tia_gain = [settings tiaGainValueForSetting:settings.tiaGain];
        // convert reading
        reading = ( (reading - _currentABaseline.floatValue) / 0.37736 / (tia_gain * settings.responseRatio) ) * 1E9;
        if(reading < 0.0f) reading = 0.0f;

        self.oxaPortAReadingLabel.text = [NSString stringWithFormat:@"%@%.04fPPM",moduleALabelString,reading];
    }
    else
    {
        // get settings
        VOXDeviceSettingsProfile *settings = [[VOXDeviceSettingsProfile alloc] initWithSubModule:connectedNode.module_b_subtype];
        float tia_gain = [settings tiaGainValueForSetting:settings.tiaGain];
        // convert reading
        reading = ( (reading - _currentBBaseline.floatValue) / 0.37736 / (tia_gain * settings.responseRatio) ) * 1E9;
        if(reading < 0.0f) reading = 0.0f;
        
        self.oxaPortBReadingLabel.text = [NSString stringWithFormat:@"%@%.04fPPM",moduleBLabelString,reading];
    }
}





@end
