//
//  VTThermaViewController.m
//  node-demo-2
//
//  Created by Wade Gasior on 10/24/12.
//  Copyright (c) 2012 Variable Technologies. All rights reserved.
//

#import "VTThermaViewController.h"
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>

@interface VTThermaViewController () <NodeDeviceDelegate>

@end

@implementation VTThermaViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    //Grab the Node delegate
    [VTNodeManager getInstance].selectedNodeDevice.delegate = self;
    if ([[VTNodeManager getInstance].selectedNodeDevice isTherma2])
    {
        [[VTNodeManager getInstance].selectedNodeDevice t2RequestOledState];
        [self updateT2OledScale:VTNodeTemperatureScaleFahrenheit];
    }
    else
        [[VTNodeManager getInstance].selectedNodeDevice setStreamModeIRTherma:YES withLedPower:YES withEmissivity:0.95 withTimestampingEnabled:NO];
    
}

-(void) viewWillDisappear:(BOOL)animated {
    
    if ([[VTNodeManager getInstance].selectedNodeDevice isTherma2])
        [[VTNodeManager getInstance].selectedNodeDevice setStreamModeIRT2:NO emissivity:0.95 withPeriod:0 withLifetime:0];
    else
        [[VTNodeManager getInstance].selectedNodeDevice setStreamModeIRTherma:NO withLedPower:NO withEmissivity:0.95 withTimestampingEnabled:NO];
    [super viewWillDisappear:animated];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NodeDeviceDelegate
-(void)nodeDeviceDidUpdateIRThermaReading:(VTNodeDevice *)device withReading:(float)reading {
    self.temperatureLabel.text = [NSString stringWithFormat:@"%.2f C", reading];
}

#pragma mark - T2 Specific

-(void)t2IRButtonPushed:(VTNodeDevice *)device
{
    NSLog(@"IR Button Pressed");
}
-(void)t2ProbeButtonPushed:(VTNodeDevice *)device
{
    NSLog(@"Probe Button Pressed");
}
-(void)t2DidTransmitTemperatureScale:(VTNodeDevice *)device scale:(VTNodeTemperatureScale)scale
{
    switch (scale) {
        case VTNodeTemperatureScaleCelcius:
            NSLog(@"Celcius");
            break;
        case VTNodeTemperatureScaleFahrenheit:
            NSLog(@"Fahrenheit");
        default:
            break;
    }
}
-(void)t2DidTransmitOledState:(VTNodeDevice *)device isOn:(BOOL)isOn
{
    NSLog(@"T2 OLED State: %d", isOn);
    if (!isOn){
        [[VTNodeManager getInstance].selectedNodeDevice t2SetOledState:true];
        NSLog(@"Turning on OLED, will start streaming in 3 seconds...");
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            //Request IR Stream after showing current OLED screen on the T2 for a few seconds...
            [[VTNodeManager getInstance].selectedNodeDevice setStreamModeIRT2:YES emissivity:0.95 withPeriod:1 withLifetime:0];
        });
    }else{
        //Request IR Stream
        [[VTNodeManager getInstance].selectedNodeDevice setStreamModeIRT2:YES emissivity:0.95 withPeriod:1 withLifetime:0];
    }
}

-(void)updateT2OledScale:(VTNodeTemperatureScale)scale
{
    //Update scale used by T2 OLED Screen to F or C
    [[VTNodeManager getInstance].selectedNodeDevice t2SetTemperatureScale:scale];
    [[VTNodeManager getInstance].selectedNodeDevice t2RequestTemperatureScale];
}


@end
