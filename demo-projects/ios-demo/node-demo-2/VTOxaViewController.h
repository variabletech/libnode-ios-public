//
//  VTOxaViewController.h
//  node-api-demo-public
//
//  Created by Andrew T on 10/3/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTOxaViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *oxaPortAReadingLabel;
@property (strong, nonatomic) IBOutlet UILabel *oxaPortBReadingLabel;

@end
