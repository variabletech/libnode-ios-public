//
//  VTChromaViewController.m
//  node-demo-2
//
//  Created by Wade Gasior on 11/14/12.
//  Updated/Maintained by Andrew Temple - 08/02/13
//  Copyright (c) 2013 Variable, Inc. All rights reserved.
//


/*
 
 Chroma API Changes/Updates for API 5:
    Chroma now uses callback blocks.
        No Delegates, No ChromaDevice objects, etc.
    Colors now represented with VIColorReading object. 
        Get a lab/xyz/etc color from Color Reading using the appropriate get methods. 
        Note the "Use Adjusted" Parameter. 
            Set this to true for color accurate readings.
            Note that unadjusted is what is used internally for color lookup.
 
    Required Flow:
    1. When NODE device is ready, initialize Chroma using this call on NodeDevice:
        chromaInitWithAppKey:success:fail:
    2. Upon successful chroma init, get calibrate and get scans using these calls on NodeDevice:
        requestChromaWhitePointCal: //Calibration with Success callback
        requestChromaReadingForObserver:temp:fail //Chroma Scan (with VIColorReading, Temperature in ˚C, Fail callback
 */

#import "VTChromaViewController.h"

#define API_KEY @"YOUR_API_KEY"
#define SRGB255(v) ((v*255>255)? 255 : (v<0 ? 0: v*255))


@interface VTChromaViewController () <NodeDeviceDelegate>
{
    UIActivityIndicatorView *activityView;
}
@property (strong, nonatomic) VTXYZColor *leftColor;
@property (strong, nonatomic) VTXYZColor *rightColor;
@end

@implementation VTChromaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self scanButtonsDisable];
}

- (void)viewDidAppear:(BOOL)animated
{
    VTNodeDevice *node = [VTNodeManager getInstance].selectedNodeDevice;
    node.delegate = self;
    
    self.statusView.hidden =NO;
    self.whitepointCalibrationButton.hidden = YES;
    [self.whitepointCalibrationButton setTitle:@"Whitepoint Cal-Waiting on Chroma Init" forState:UIControlStateNormal];
    self.whitepointCalibrationButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.whitepointCalibrationButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.whitepointCalibrationButton setTitle:@"Perform\nWhitepoint\nCalibration" forState:UIControlStateNormal];
    
    NSLog(@"Starting Chroma session");
    [VIChroma startWithKey:API_KEY onSuccess:^(VIChromaSession *session) {
        NSLog(@"chroma API started");
        [node colorSensorInit:^(BOOL chromaReady) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //we're ready to start scanning
                NSLog(@"color sensor initialized");
                self.statusView.hidden = YES;
                self.whitepointCalibrationButton.hidden = NO;
                [self.whitepointCalibrationButton setTitle:@"Perform\nWhitepoint\nCalibration" forState:UIControlStateNormal];
                [self scanButtonsEnable];
            });
        } onFail:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //uh oh
                self.handShakeDetailLabel.text = @"Color Sensor Initialization Failed...";
            });
        }];
    } onError:^(NSError *error) {
        NSLog(@"Error starting Chroma API: %@", error);
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark ------- User I/O -------


- (IBAction)whitepointCalibrationButtonTapped:(id)sender
{
    VTNodeDevice* node = [[VTNodeManager getInstance] selectedNodeDevice];
    if([node hasSensorWithModuleType:VTNodeModuleTypePhoton]){
        [node requestPhotonWhitepointCal];
    }else if([node hasSensorWithModuleType:VTNodeModuleTypeChroma]){
        [[VTNodeManager getInstance].selectedNodeDevice requestChromaWhitePointCal:^(BOOL success) {
            [self onCalibrationWithResults:success];
        }];
    }
}

NSUInteger scanRequestSide = 0;

- (IBAction)scanButtonRightTapped:(id)sender
{
    scanRequestSide = 1;
    [self scanButtonsDisable];
    [self requestColorScan];
}

- (IBAction)scanButtonLeftTapped:(id)sender
{
    scanRequestSide = 2;
    [self scanButtonsDisable];
    [self requestColorScan];
}

-(void)updateView:(UIView*)view label:(UILabel*)label withColor:(VTXYZColor*)reading
{
    UIColor * color = [[reading RGBColorInSpace:VTColorUtilsRGBSpace_sRGB] color];
    [view setBackgroundColor:color];
    CGFloat r,g,b;
    [view.backgroundColor getRed:&r green:&g blue:&b alpha:nil];
    label.text = [NSString stringWithFormat:@"RGB\n%.0f,%.0f,%.0f",SRGB255(r),SRGB255(g),SRGB255(b)];
    if(self.leftColor != nil && self.rightColor != nil) {
        [self deltaE2000];
    }
    [self scanButtonsEnable];
}

- (void)scanButtonsEnable
{
    self.scanButtonLeft.enabled = YES;
    self.scanButtonLeft.alpha = 1.0f;
    self.scanButtonRight.enabled = YES;
    self.scanButtonRight.alpha = 1.0f;
}

- (void)scanButtonsDisable
{
    self.scanButtonLeft.enabled = NO;
    self.scanButtonLeft.alpha = 0.4f;
    self.scanButtonRight.enabled = NO;
    self.scanButtonRight.alpha = 0.4f;
}

#pragma mark ------- Process Color Readings -------



-(void) deltaE2000
{
    VTLabColor *left = [self.leftColor toLab];
    VTLabColor *right = [self.rightColor toLab];
    double deltaE = [left deltaE00:right];
    self.colorCompareInfo.text = [NSString stringWithFormat: @"Delta E:\n%.2f",deltaE];//color difference. Refer to here for background:https://en.wikipedia.org/wiki/Color_difference
}



#pragma mark --------- Request Color Scan --------
- (void) requestColorScan{
    VTNodeDevice* node = [[VTNodeManager getInstance] selectedNodeDevice];
    if([node hasSensorWithModuleType:VTNodeModuleTypeChroma]){
        [node requestChromaReadingForObserver:VTColorUtilsObserver_1931_2deg
                                        color:^(VIColorReading *reading, NSString *analyticsUUID) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                VTXYZColor* xyzColor = [reading getXYZ:VTColorUtilsIlluminantD50 adjusted:true];
                                                [self onColorScannedWithColor:xyzColor];
                                            });
                                            
                                        } temp:^(float temperature) {
                                            NSLog(@"Temperature seen by chroma: %f",temperature);
                                        } fail:^(NSError *error) {
                                            NSLog(@"Error in chroma reading: %@",[error localizedDescription]);
                                        }];
    }else if([node hasSensorWithModuleType:VTNodeModuleTypePhoton]){
        [node requestSpectrum];
    }
}


#pragma Event Handlers
- (void) onColorScannedWithColor: (VTXYZColor*) reading{
    if(scanRequestSide == 1){
        self.rightColor = reading;
        [self updateView:self.colorDisplayRight label:self.colorInfoRight withColor:reading];
    }else if(scanRequestSide == 2){
        self.leftColor = reading;
        [self updateView:self.colorDisplayLeft label:self.colorInfoLeft withColor:reading];
    }
    
    scanRequestSide = 0;

}

- (void) onCalibrationWithResults: (BOOL) calibrationResult{
    NSLog(@"Whitepoint Cal completed with status: %d",calibrationResult);
    if (calibrationResult)
        self.whitepointCalibrationButton.hidden = YES;
    else
    {
        [self.whitepointCalibrationButton setTitle:@"Cal Failed, Retry" forState:UIControlStateNormal];
        [self.whitepointCalibrationButton setTitle:@"Cal Failed, Retry" forState:UIControlStateSelected];
    }}

/**
 * Invoked when a Node device has transmitted a complete photon pixle reading. This is not invoked if the reading recieved is a calibration reading.
 *
 * @param reading - the reading transmitted from a node device.
 **/
-(void) nodeDeviceDidTransmitPixelsWithReading: (VIPhotonPixelReading*) reading{
    VTXYZColor* color =    [[reading toSpectralColor] toXYZ];
    [self onColorScannedWithColor:color];
}

/**
 * Invoked when a photon is white point calibrated.
 *
 * @param - calibrationResult true, if the calibration was a success
 * @param - reading the calibration reading.
 */
-(void) nodeDeviceDidWhitePointCalibratePhoton: (bool) calibrationResults withReading: (VIPhotonPixelReading*) reading{
    NSLog(@"nodeDeviceDidWhitepointCalibratePhoton");
    [self onCalibrationWithResults:calibrationResults];
}

@end
