//
//  VOXDeviceSettingsProfile.m
//  NODE Oxa
//
//  Created by Tyler Brown on 7/23/13.
//  Copyright (c) 2013 Variable Inc. All rights reserved.
//

#import "VOXDeviceSettingsProfile.h"

@implementation VOXDeviceSettingsProfile

@synthesize subModule = _subModule;

-(id) init {
    return [self initWithSubModule:VTNodeModuleSubTypeCO];
}

-(id) initWithSubModule:(VTNodeModuleSubType)subModule {
    if(self = [super init]) {
        self.subModule = subModule;
    }
    return self;
}


#pragma mark - Helper

-(float) tiaGainValueForSetting:(int)tiaSetting {
    if(tiaSetting == 6) return 120000.f;
    if(tiaSetting == 4) return 14000.f;
    if(tiaSetting == 1) return 2750.f;
    if(tiaSetting == 5) return 35000.f;
    if(tiaSetting == 7) return 350000.f;
    if(tiaSetting == 2) return 3500.f;
    if(tiaSetting == 3) return 7000.f;
    if(tiaSetting == 8) return 1.f;
    return 1.f;
}



// ===================================
#pragma mark - CUSTOM GETTERS & SETTERS
// ===================================

-(void) setSubModule:(VTNodeModuleSubType)subModule {
    _subModule = subModule;
}

-(int) tiaGain {
    if(_subModule == VTNodeModuleSubTypeCO)  return 5;
    if(_subModule == VTNodeModuleSubTypeCL2) return 6;
    if(_subModule == VTNodeModuleSubTypeSO2) return 7;
    if(_subModule == VTNodeModuleSubTypeNO2) return 6;
    if(_subModule == VTNodeModuleSubTypeH2S) return 7;
    if(_subModule == VTNodeModuleSubTypeNO)  return 5;
    if(_subModule == VTNodeModuleSubTypeCO2) return 8;
    return -1;
}

-(int) intZ {
    if(_subModule == VTNodeModuleSubTypeCO)  return 0;
    if(_subModule == VTNodeModuleSubTypeCL2) return 2;
    if(_subModule == VTNodeModuleSubTypeSO2) return 0;
    if(_subModule == VTNodeModuleSubTypeNO2) return 0;
    if(_subModule == VTNodeModuleSubTypeH2S) return 2;
    if(_subModule == VTNodeModuleSubTypeNO)  return 0;
    return -1;
}

-(int) refSource {
    return 1;
}

-(int) biasSign {
    return 1;
}

-(int) biasValue {
    return 0;
}

-(int) rLoad {
    return 0;
}

-(int) opMode {
    return 3;
}

-(int) shortingEFT {
    return 0;
}

-(float) responseRatio {
    if(_subModule == VTNodeModuleSubTypeCO)  return 39;
    if(_subModule == VTNodeModuleSubTypeCL2) return -200;
    if(_subModule == VTNodeModuleSubTypeSO2) return 265;
    if(_subModule == VTNodeModuleSubTypeNO2) return -350;
    if(_subModule == VTNodeModuleSubTypeH2S) return 135;
    if(_subModule == VTNodeModuleSubTypeNO)  return 625;
    if(_subModule == VTNodeModuleSubTypeCO2) return 2649989400.0424f;
    return -1;
}

@end

/*
 typedef enum {VTOxaTiaGainSettingExt, VTOxaTiaGainSetting2p75, VTOxaTiaGainSetting3p5, VTOxaTiaGainSetting7, VTOxaTiaGainSetting14, VTOxaTiaGainSetting35, VTOxaTiaGainSetting120, VTOxaTiaGainSetting350} VTOxaTiaGainSetting;
 typedef enum {VTOxaRLoadSetting10, VTOxaRLoadSetting33, VTOxaRLoadSetting50, VTOxaRLoadSetting100} VTOxaRLoadSetting;
 typedef enum {VTOxaRefSourceSettingInternal, VTOxaRefSourceSettingExternal} VTOxaRefSourceSetting;
 typedef enum {VTOxaIntZSetting20, VTOxaIntZSetting50, VTOxaIntZSetting67, VTOxaIntZSettingBypass} VTOxaIntZSetting;
 typedef enum {VTOxaBiasSignSettingNegative, VTOxaBiasSignSettingPositive} VTOxaBiasSignSetting;
 typedef enum {VTOxaShortingEFTSettingDisabled, VTOxaShortingEFTSettingEnabled} VTOxaShortingEFTSetting;
 typedef enum {VTOxaBiasValueSetting0, VTOxaBiasValueSetting1, VTOxaBiasValueSetting2, VTOxaBiasValueSetting4, VTOxaBiasValueSetting6, VTOxaBiasValueSetting8, VTOxaBiasValueSetting10, VTOxaBiasValueSetting12, VTOxaBiasValueSetting14, VTOxaBiasValueSetting16, VTOxaBiasValueSetting18, VTOxaBiasValueSetting20, VTOxaBiasValueSetting22, VTOxaBiasValueSetting24} VTOxaBiasValueSetting;
 typedef enum {VTOxaOpModeSetting2LeadGalvanic, VTOxaOpModeSetting3LeadAmperometric, VTOxaOpModeSettingTempMeasureTiaOff, VTOxaOpModeSettingTempMeasureTiaOn} VTOxaOpModeSetting;
*/

