//
//  VTColorMatchViewController.h
//  node-api-demo-public
//
//  Created by Andrew T on 7/13/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeManager.h"
#import <Node_iOS/Node_iOS.h>
#import "VTColorMatchTableViewCell.h"

@interface VTColorMatchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *viewColorMatching;
- (IBAction)btnCalPressed:(id)sender;
- (IBAction)btnScanPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end
