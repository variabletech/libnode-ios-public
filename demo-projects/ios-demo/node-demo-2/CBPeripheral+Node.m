//
//  CBPeripheral+Node.m
//  node-api-demo-public
//
//  Created by Tyler Brown on 5/31/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//

#import "CBPeripheral+Node.h"

static char const * const kRssiAtDiscoveryKey = "kRssiAtDiscoveryKey";
static char const * const kIsT3ProbeOnly = "kIsT3ProbeOnly";

@implementation CBPeripheral (Node)

-(NSNumber*) rssiAtDiscovery {
    // default if nil before returning
    NSNumber *rssi = objc_getAssociatedObject(self, kRssiAtDiscoveryKey);
    if(rssi == nil) rssi = [NSNumber numberWithInt:-120];
    return rssi;
}

-(void) setRssiAtDiscovery:(NSNumber*)rssiAtDiscovery {
    objc_setAssociatedObject(self, kRssiAtDiscoveryKey, rssiAtDiscovery, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


-(NSNumber*) isT3ProbeOnly {
    // default if nil before returning
    NSNumber *value = objc_getAssociatedObject(self, kIsT3ProbeOnly);
    if(value == nil) value = [NSNumber numberWithBool:false];
    return value;
}

-(void) setIsT3ProbeOnly:(NSNumber*)value {
    objc_setAssociatedObject(self, kIsT3ProbeOnly, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
