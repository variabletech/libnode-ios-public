//
//  VTColorBrowseViewController.m
//  node-api-demo-public
//
//  Created by Andrew T on 7/14/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import "VTColorBrowseViewController.h"
#import "VTColorMatchViewController.h"

typedef NS_ENUM(NSUInteger, CurrentDataSet){
    LIBRARIES,
    SWATCHES
};

@interface VTColorBrowseViewController ()
{
    CurrentDataSet currentDataSet;
    NSArray *swatches;
}
@end

@implementation VTColorBrowseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    currentDataSet = LIBRARIES;
    [self.tableView registerNib:[UINib nibWithNibName:@"VTColorMatchTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    UIBarButtonItem *btnLibraries = [[UIBarButtonItem alloc] initWithTitle:@"Show Libs"
                                                                     style:UIBarButtonItemStylePlain target:self action:@selector(switchDataSet)];
     self.navigationItem.rightBarButtonItem = btnLibraries;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)switchDataSet
{
    if (currentDataSet == LIBRARIES){}
    else{
        currentDataSet = LIBRARIES;
        [self.tableView reloadData];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (currentDataSet == LIBRARIES)
        return [self.myLibraries count];
    else
        return [swatches count];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VTColorMatchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil){
        cell = [[VTColorMatchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (currentDataSet == LIBRARIES){
        VIChromaMatchLibrary *lib = self.myLibraries[indexPath.row];
        cell.lblLibrary.text = [NSString stringWithFormat:@"%@:%@",lib.vendorName, lib.name];
        cell.lblColorName.text = @"Tap for swatch list";
        cell.lblDeltaE.text = @"";
        NSInteger aRedValue = arc4random()%255;
        NSInteger aGreenValue = arc4random()%255;
        NSInteger aBlueValue = arc4random()%255;
        UIColor *randColor = [UIColor colorWithRed:aRedValue/255.0f green:aGreenValue/255.0f blue:aBlueValue/255.0f alpha:1.0f];
        cell.viewColor.backgroundColor = randColor;
    }
    else{
        VIChromaMatchSwatch *swatch = swatches[indexPath.row];
        cell.lblLibrary.text = swatch.name;
        cell.lblColorName.text = [NSString stringWithFormat:@"%@:%@,%@", swatch.code, swatch.library.name, swatch.library.vendorName];
        cell.lblDeltaE.text = @"";
        cell.viewColor.backgroundColor = [UIColor colorWithRed:swatch.R/255.0 green:swatch.G/255.0 blue:swatch.B/255.0 alpha:1.0];
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (currentDataSet == LIBRARIES){
        VIChromaMatchLibrary *lib = self.myLibraries[indexPath.row];
        swatches = [lib fetchSwatches];
        currentDataSet = SWATCHES;
        [self.tableView reloadData];
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}

@end
