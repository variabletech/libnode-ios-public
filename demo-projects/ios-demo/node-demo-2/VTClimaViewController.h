//
//  VTClimaViewController.h
//  node-demo-2
//
//  Created by Wade Gasior on 10/24/12.
//  Copyright (c) 2012 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>

@interface VTClimaViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *lightLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;

@end
