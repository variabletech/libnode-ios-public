//
//  VTColorBrowseViewController.h
//  node-api-demo-public
//
//  Created by Andrew T on 7/14/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTColorBrowseViewController : UITableViewController
@property (strong, nonatomic) NSArray *myLibraries;
@end
