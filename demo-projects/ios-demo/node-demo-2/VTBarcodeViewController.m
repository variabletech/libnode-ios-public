//
//  VTBarcodeViewController.m
//  node-api-demo-public
//
//  Created by Andrew T on 6/23/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import "VTBarcodeViewController.h"
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>

@interface VTBarcodeViewController () <NodeDeviceDelegate>

@end

@implementation VTBarcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtScan.delegate = self;
    UIView* dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    self.txtScan.inputView = dummyView; // Hide keyboard, but show blinking cursor
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    //Grab the Node delegate
    [VTNodeManager getInstance].selectedNodeDevice.delegate = self;
    [[VTNodeManager getInstance].selectedNodeDevice requestBarcodeButtonStatus];
}


-(void)nodeDeviceDidUpdateBarcodeButtonState:(VTNodeDevice *)device enabled:(BOOL)enabled
{
    [self.switchEnableNodeBtnBarcode setOn:enabled];
}

-(void)nodeDeviceDidUpdateBarcodeReading:(VTNodeDevice *)device withReading:(NSString *)reading
{
    [self.txtScan setText:reading];
}


- (IBAction)btnScanPressed:(id)sender {
    [[VTNodeManager getInstance].selectedNodeDevice requestBarcodeReading];
}

- (IBAction)switchEnableNodeBtnChanged:(id)sender {
    [[VTNodeManager getInstance].selectedNodeDevice requestBarcodeButtonEnabled:self.switchEnableNodeBtnBarcode.isOn];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return false;
}


@end
