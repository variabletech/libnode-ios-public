//
//  IOUARTViewController.m
//  node-api-demo-public
//
//  Created by Andrew T on 10/1/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//

/******************************************************************************************************************
 IO Module Details:
 The IO Module contains:
 -8 GPIO pins
 -2 A2D inputs
 -4 pins which currently are set up to be used for UART
 
 Functionality:
 UART is initialized using the VTNodeDevice's requestIoUart:flowControl.
 Due to BT Low Energy throughput speeds, selecting UART baud > 9600 may result in lost data
 
 UART messages are transmitted using the sendIOUart:buffer: method, which handles binary or ASCII data IO
 UART messages are received / provided with VTNodeDevice's Delegate method: nodeDevice didTransmitIOModuleUartPacket:withPayloadSize
 
 This class will provide a basic I/O scheme at 9600 baud, and send/receive 
 
 ******************************************************************************************************************/



#import "IOUARTViewController.h"

@interface IOUARTViewController ()
{
    VTNodeDevice *connectedNode;
}
@end

@implementation IOUARTViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    connectedNode =[VTNodeManager getInstance].selectedNodeDevice;
    self.uartCommandField.delegate = self;
    self.uartIOTextField.layer.borderColor = [[UIColor grayColor] CGColor];;
    self.uartIOTextField.layer.borderWidth =5.0f;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    connectedNode.delegate = self;
    [connectedNode requestIoUart:9600 flowControl:NO];
}


-(void) viewWillDisappear:(BOOL)animated {
    [connectedNode requestIoUart:NO flowControl:NO];
    [super viewWillDisappear:animated];
}


#pragma mark ---- NODE Delegate Methods ----

-(void)nodeDevice:(VTNodeDevice *)device didTransmitIOModuleUartPacket:(NSData *)packet withPayloadSize:(uint8_t)payloadSize
{
    NSString *uartAscii = [[NSString alloc] initWithData:packet encoding:NSASCIIStringEncoding];;
    NSLog(@"IO Module UART Packet: %@",uartAscii);
    self.uartIOTextField.text = [self.uartIOTextField.text stringByAppendingString:[NSString stringWithFormat:@"%@\n",uartAscii]];
}




#pragma mark ---- UITextView Delegate ----
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    self.uartCommandField.text = @"";
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [connectedNode sendIOUart:(uint32_t)[self.uartCommandField.text length] buffer:[self.uartCommandField.text dataUsingEncoding:NSASCIIStringEncoding]];
    self.uartIOTextField.text = [self.uartIOTextField.text stringByAppendingString:[NSString stringWithFormat:@"%@\n",self.uartCommandField.text]];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([string isEqualToString:@"\n"]) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

- (IBAction)dismissCommandPressed:(id)sender {
    [self.uartCommandField resignFirstResponder];
}




- (IBAction)sendCommand:(id)sender {
    [self dismissCommandPressed:nil];
}
@end
