//
//  VTChromaViewController.h
//  node-demo-2
//
//  Created by Wade Gasior on 11/14/12.
//  Copyright (c) 2012 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>
#import <Node_iOS/Node_iOS.h>

@interface VTChromaViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *colorDisplayLeft;
@property (weak, nonatomic) IBOutlet UIView *colorDisplayRight;
@property (weak, nonatomic) IBOutlet UILabel *colorInfoLeft;
@property (weak, nonatomic) IBOutlet UILabel *colorInfoRight;
@property (weak, nonatomic) IBOutlet UILabel *colorCompareInfo;
@property (weak, nonatomic) IBOutlet UIButton *scanButtonLeft;
@property (weak, nonatomic) IBOutlet UIButton *scanButtonRight;

@property (strong, nonatomic) IBOutlet UIView *statusView;
@property (strong, nonatomic) IBOutlet UILabel *handShakeDetailLabel;

- (IBAction)scanButtonRightTapped:(id)sender;
- (IBAction)scanButtonLeftTapped:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *whitepointCalibrationButton;
- (IBAction)whitepointCalibrationButtonTapped:(id)sender;


@end
