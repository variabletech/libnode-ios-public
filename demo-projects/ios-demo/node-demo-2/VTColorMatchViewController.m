//
//  VTColorMatchViewController.m
//  node-api-demo-public
//
//  Created by Andrew T on 7/13/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import "VTColorMatchViewController.h"
#import "VTColorBrowseViewController.h"

#define API_KEY @"YOUR_API_KEY"
#define COLOR_KEY @"YOUR_COLOR_KEY"

@interface VTColorMatchViewController () <NodeDeviceDelegate>
{
    NSArray *matchingLibraries;
    NSArray *matchingResults;
    VIColorReading *colorScan;
}
@end

@implementation VTColorMatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [VTNodeManager getInstance].selectedNodeDevice.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:[UINib nibWithNibName:@"VTColorMatchTableViewCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    self.viewColorMatching.hidden = true;
    
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(135,140,50,50)];
    spinner.color = [UIColor blueColor];
    [spinner startAnimating];
    [self.view addSubview:spinner];
    
    [VIChroma startWithKey:API_KEY andColorLibraryKey:COLOR_KEY onSuccess:^(VIChromaSession *session) {
        NSLog(@"Chroma session started.");
        [[VTNodeManager getInstance].selectedNodeDevice chromaInit:^(BOOL chromaReady) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Chroma device ready.");
                [self hideSpinner:spinner];
                [self getLibrariesAfterValidatingAccount];
            });
        } onFail:^(NSError *error) {
            NSLog(@"Failed to init chroma: %@", error);
        }];
    } onError:^(NSError *error) {
        NSLog(@"Failed to start chroma session: %@", error);
    }];
}

-(void)alertWithTitle:(NSString*)title message:(NSString*)msg
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:ac animated:true completion:nil];
}

-(void)hideSpinner:(UIActivityIndicatorView*)spinner
{
    dispatch_async(dispatch_get_main_queue(), ^{
        // stop and remove the spinner on the background when done
        [spinner removeFromSuperview];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - NODE Chroma Match Access

-(void)getLibrariesAfterValidatingAccount
{
    self.viewColorMatching.hidden = false;
    [self alertWithTitle:@"API Key Validated" message:@"Ready to match with your color libraries"];
    matchingLibraries = [VIChromaMatch fetchAllLibraries];
    NSLog(@"Libraries available for matching: ");
    NSString *chromaSerial;
    if ([VTNodeManager getInstance].selectedNodeDevice.module_a_type == VTNodeModuleTypeChroma)
        chromaSerial =[VTNodeManager getInstance].selectedNodeDevice.moduleASerial;
    else
        chromaSerial =[VTNodeManager getInstance].selectedNodeDevice.moduleBSerial;
    for (VIChromaMatchLibrary *lib in matchingLibraries){
        NSLog(@"   %@, Is this library available for matching? %d", lib.name, [lib isCompatibleWithChromaSerial:chromaSerial] );
    }
}


#pragma mark - Chroma Scanning Methods:

- (IBAction)btnCalPressed:(id)sender {
    [[VTNodeManager getInstance].selectedNodeDevice requestChromaWhitePointCal:^(BOOL success) {
        NSLog(@"Whitepoint Cal completed with status: %d",success);
        if (success)
            [self alertWithTitle:@"Calibration Succeeded" message:@"Scan on..."];
        else
        {
            [self alertWithTitle:@"Calibration Failed" message:@"Check the cap is on your chroma and try again"];
        }
    }];
}

- (IBAction)btnScanPressed:(id)sender {
    VTNodeDevice *node = [VTNodeManager getInstance].selectedNodeDevice;
    [node requestChromaMatchReadingUsingLibraries:[VIChromaMatch fetchAllLibraries] onResult:^(NSArray *results, VIColorReading *reading, NSString *eventUUID) {
        matchingResults = [results copy];
        colorScan = reading;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    }];
}


#pragma mark - TableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0){
        if (colorScan != nil) return 1;
        else return 0;
    }else{
        if (matchingResults == nil) return 0;
        else return [matchingResults count];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //Initial Section will show the actual scan info, 2nd section will show Color Match results
    return 2;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VTColorMatchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    if (cell == nil){
        cell = [[VTColorMatchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (indexPath.section == 0){
        cell.lblLibrary.text = @"Chroma Scan Color";
        VTLabColor *lab = [colorScan getLab:VTColorUtilsIlluminantD50 adjusted:true];
        cell.lblColorName.text = [NSString stringWithFormat:@"CIE Lab: %.02f, %.02f, %.02f",lab.L,lab.A,lab.B];
        cell.lblDeltaE.text = @"";
        cell.viewColor.backgroundColor = [colorScan displayColor];
    }
    else{
        VIChromaMatchResult *result = matchingResults[indexPath.row];
        cell.lblLibrary.text = result.swatch.library.name;
        cell.lblColorName.text = result.swatch.name;
        cell.lblDeltaE.text = [NSString stringWithFormat:@"%.02f",result.deltaEFromReading];
        cell.viewColor.backgroundColor = [UIColor colorWithRed:result.swatch.R/255.0 green:result.swatch.G/255.0 blue:result.swatch.B/255.0 alpha:1.0];
    }
    return cell;
}

-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 1)
        return @"Color Matching Results:";
    return @"";
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
        return 30.0;
    return 0.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0;
}


#pragma mark - navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    VTColorBrowseViewController *dest = [segue destinationViewController];
    dest.myLibraries = matchingLibraries;
}


@end
