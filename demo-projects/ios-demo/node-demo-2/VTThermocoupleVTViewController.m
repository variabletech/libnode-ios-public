//
//  VTThermocoupleVTViewController.m
//  node-api-demo-public
//
//  Created by Andrew T on 6/30/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import "VTThermocoupleVTViewController.h"
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>


@interface VTThermocoupleVTViewController ()<NodeDeviceDelegate>
@property(assign) BOOL isStreaming;
@end

@implementation VTThermocoupleVTViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated {
    //Grab the Node delegate
    [VTNodeManager getInstance].selectedNodeDevice.delegate = self;
    [[VTNodeManager getInstance].selectedNodeDevice setStreamModeThermocouple:YES withPeriod:20 withLifetime:0];
  self.isStreaming = true;
}

-(void) viewWillDisappear:(BOOL)animated {
    [[VTNodeManager getInstance].selectedNodeDevice setStreamModeThermocouple:NO withPeriod:20 withLifetime:0];
    [super viewWillDisappear:animated];
}



#pragma mark - NODE Delegate

-(void)nodeDeviceDidTransmitThermocoupleReading:(VTNodeDevice *)device withReading:(float)reading
{
    [self.temperatureLabel setText:[NSString stringWithFormat:@"%.02f C", reading]];
}


#pragma mark - T2 Specific

- (void)thermaOnZeroProbe:(VTNodeDevice *)device success:(BOOL)success code:(uint8_t)errorCode{
  NSString *title = success ? @"Calibration Success" : @"Calibration Error";
  NSString *msg = [NSString stringWithFormat:@"Calibration Response Code: 0x%02X", errorCode];
  UIAlertController *ac = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
  [ac addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
  dispatch_async(dispatch_get_main_queue(), ^{
    [self presentViewController:ac animated:true completion:nil];
  });
}

-(void)t2IRButtonPushed:(VTNodeDevice *)device
{
    NSLog(@"IR Button Pressed");
}
-(void)t2ProbeButtonPushed:(VTNodeDevice *)device
{
    NSLog(@"Probe Button Pressed");
}
-(void)t2DidTransmitTemperatureScale:(VTNodeDevice *)device scale:(VTNodeTemperatureScale)scale
{
    switch (scale) {
        case VTNodeTemperatureScaleCelcius:
            NSLog(@"Celcius");
            break;
        case VTNodeTemperatureScaleFahrenheit:
            NSLog(@"Fahrenheit");
        default:
            break;
    }
}

-(void)updateT2OledScale:(VTNodeTemperatureScale)scale
{
    //Update scale used by T2 OLED Screen to F or C
    [[VTNodeManager getInstance].selectedNodeDevice t2SetTemperatureScale:scale];
    [[VTNodeManager getInstance].selectedNodeDevice t2RequestTemperatureScale];
}


- (IBAction)onZeroProbe:(id)sender {
  if (!self.isStreaming){
    //zero probe when not actively streaming:
    [[VTNodeManager getInstance].selectedNodeDevice thermaZeroProbe];
  }
}

- (IBAction)onToggleStream:(id)sender {
  if (self.isStreaming){
    [[VTNodeManager getInstance].selectedNodeDevice setStreamModeThermocouple:NO withPeriod:20 withLifetime:0];
    self.isStreaming = false;
  } else {
    [[VTNodeManager getInstance].selectedNodeDevice setStreamModeThermocouple:YES withPeriod:20 withLifetime:0];
    self.isStreaming = true;
  }
}
@end
