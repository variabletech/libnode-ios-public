//
//  VTIOModuleViewController.h
//  api-demo-io_module
//
//  Created by Andrew T on 9/25/13.
//  Copyright (c) 2013 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTNodeManager.h"

#import <Node_iOS/Node.h>

@interface VTIOModuleViewController: UIViewController <NodeDeviceDelegate,UIAlertViewDelegate>

//D7-D0 controls
@property (strong, nonatomic) IBOutlet UIButton *io7SettingsButton;
- (IBAction)io7SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io7Switch;

@property (strong, nonatomic) IBOutlet UIButton *io6Button;
- (IBAction)io6SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io6Switch;

@property (strong, nonatomic) IBOutlet UIButton *io5Setting;
- (IBAction)io5SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io5Switch;

@property (strong, nonatomic) IBOutlet UIButton *io4Setting;
- (IBAction)io4SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io4Switch;

@property (strong, nonatomic) IBOutlet UIButton *io3Setting;
- (IBAction)io3SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io3Switch;

@property (strong, nonatomic) IBOutlet UIButton *io2Setting;
- (IBAction)io2SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io2Switch;

@property (strong, nonatomic) IBOutlet UIButton *io1Setting;
- (IBAction)io1SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io1Switch;

@property (strong, nonatomic) IBOutlet UIButton *io0Setting;
- (IBAction)io0SettingPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UISwitch *io0Switch;

- (IBAction)gpioSwitchesUpdated:(id)sender;


@property (strong, nonatomic) IBOutlet UISlider *a0Slider;
@property (strong, nonatomic) IBOutlet UISlider *a1Slider;



@end
