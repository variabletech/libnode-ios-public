//
//  VTThermocoupleVTViewController.h
//  node-api-demo-public
//
//  Created by Andrew T on 6/30/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTThermocoupleVTViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
- (IBAction)onToggleStream:(id)sender;
- (IBAction)onZeroProbe:(id)sender;

@end
