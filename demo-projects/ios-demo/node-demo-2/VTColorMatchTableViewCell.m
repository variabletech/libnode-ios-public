//
//  VTColorMatchTableViewCell.m
//  node-api-demo-public
//
//  Created by Andrew T on 7/13/15.
//  Copyright (c) 2015 Variable Technologies. All rights reserved.
//

#import "VTColorMatchTableViewCell.h"

@implementation VTColorMatchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
