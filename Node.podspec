Pod::Spec.new do |s|
  s.name         = "Node"
  s.version      = "4.2.4"
  s.summary      = "The NODE Obj-C framework allows you to develop iOS and Mac OSX applications using NODE+, a modular sensor system made by Variable."

  s.description  = <<-DESC
                   The NODE Objective-C framework allows you to develop custom iOS and Mac OSX applications using NODE+, a wireless modular sensor system made by Variable Inc.

                   More details can be found at http://www.variableinc.com/
                   DESC

  s.homepage     = 'http://variableinc.com'

  s.license      = 'http://variableinc.com/terms-use-license/'

  s.author       = { "Variable, Inc." => "customersupport@variableinc.com" }


  s.requires_arc = true

  s.source       = { :hg => "https://bitbucket.org/variabletech/libnode-ios-public" }

  s.platform = :ios, '8.0'

  s.preserve_paths = 'Node_iOS.framework'

  s.public_header_files = 'Node_iOS.framework/Versions/A/Headers/*'

  s.vendored_frameworks = 'Node_iOS.framework'

  s.frameworks = 'CoreBluetooth', 'CoreLocation', 'GLKit', 'Accelerate', 'SystemConfiguration'

  s.libraries = 'z', 'sqlite3'


end
